## yum

yum -y install gcc pcre pcre-devel zlib zlib-devel openssl openssl-devel gcc-c++



## download

wget https://www.openssl.org/source/openssl-1.1.1i.tar.gz

## configure

./configure  --with-http_ssl_module --with-pcre --with-pcre-jit --with-http_stub_status_module --with-stream --with-stream_ssl_module --with-http_v2_module --with-openssl-opt=enable-tls1_3 --with-openssl=./openssl-1.1.1i



## http

```nginx
http {
		log_format  main escape=json  '$remote_addr|$remote_user| [$time_local]| $request| $request_body | $request_time'
                      '|$status |$body_bytes_sent| $http_referer '
                      '|$http_user_agent| $http_x_forwarded_for | $http_xreferer | $upstream_addr | $http_authorization';
    charset utf-8;
    
    access_log  logs/access.log  main;

    sendfile        on;
    tcp_nopush  		on; # 启用了 sendfile 之后才生效
    tcp_nodelay 		on;
    #tcp_nopush     on;
    server_tokens off;

    #keepalive_timeout  0;
    keepalive_timeout  65;

    gzip  on;
    gzip_min_length  1k;
    gzip_buffers     4 16k;
    gzip_http_version 1.0;
    gzip_comp_level 2;
    gzip_types       text/plain text/css application/json application/x-javascript text/xml application/xml application/xml+rss text/javascript application/javascript;
    gzip_vary on;
}
```





## server

``````nginx
server {
    listen 80 reuseport fastopen=3 http2;
    server_name foo.com;

    root /path;
    index index.html index.htm index.php;
    
    ssl_prefer_server_ciphers on;  # prefer a list of ciphers to prevent old and slow ciphers
    ssl_ciphers 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH';

    ssl_stapling on;
    ssl_stapling_verify on;
    ssl_buffer_size 4k;

    # Enable SSL cache to speed up for return visitors
	  ssl_protocols TLSv1.3;
	  ssl_early_data on;
    ssl_session_cache   shared:SSL:50m; # speed up first time. 1m ~= 4000 connections
    ssl_session_timeout 60m;
    ssl_certificate /usr/local/openresty/nginx/conf/pem/www.iowms.com_chain.crt;
    ssl_certificate_key /usr/local/openresty/nginx/conf/pem/www.iowms.com_key.key;   
  	
    location / {
    		add_header Kss-Upstream $upstream_addr;
    		proxy_pass http://127.0.0.1:5950;
    }
    
    location /nginx_status {
      	stub_status;
    }
  	location /benchmark { 
        add_header Content-Type "text/plain;charset=utf-8";
        return 200 "hello, world."; 
  	}
}

``````